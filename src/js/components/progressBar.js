var progressBar = (function ($) {

	'use strict';

	var progressBar = {
		'obj': $('.progressbar'),
		'position': $('.skill-set').offset().top - 50
	};

	progressBar.animate = function (bar, target, offset) {
		if (offset) {
			bar.find('.bar span').css({'width': target, 'transition-delay': offset, 'color': 'white'}).html(target);
		}else {
			bar.find('.bar span').css({'width': target, 'color': 'transparent'});
		}
	};

	progressBar.reset = function () {
		for (var i = 0; i < progressBar.obj.length; i++) {
			var bar = $(progressBar.obj[i]);
			progressBar.animate(bar, '0%');
		}
	};

	progressBar.addAnimation = function () {
		for (var i = 0; i < progressBar.obj.length; i++) {
			var bar 	 = $(progressBar.obj[i]);
			var offset = '0.'+i+'s';
			var target = bar.data('target');
			progressBar.animate(bar, target, offset);
		}
	};

	// init
	$(window).on('scroll', function () {
		var position = $(window).scrollTop();

		if (position >= progressBar.position) {
			progressBar.addAnimation();
		}else {
			progressBar.reset();
		}
	});

	return progressBar;

})(jQuery);
