var menu = (function ($) {

	'use strict';

	var menu = {
		'obj': 		$('body > nav'),
		'btn': 		$('.menu-button'),
		'class':  'open',
		'isOpen': false
	};

	menu.open = function () {
		menu.obj.addClass(menu.class);
		menu.isOpen = true;
	};

	menu.close = function () {
		menu.obj.removeClass(menu.class);
		menu.isOpen = false;
	};

	menu.toggle = function () {
		if (menu.isOpen) {
			menu.close();
		}else {
			menu.open();
		}
	};

	menu.addTrigger = function (button, action) {
		button.on(action, function (event) {
			event.preventDefault();
			menu.toggle();
		});
	};

	// Init
	$(function () {
		menu.addTrigger(menu.btn, 'click');
	});

	return menu;

})(jQuery);
