var loadingScreen = (function ($) {

	'use strict';

	var loadingScreen = {
		'obj': $('.loading-screen'),
		'isLoading': false
	};

	loadingScreen.add = function () {
		$('body').append(loadingScreen.obj);
		loadingScreen.obj.hide().fadeIn('fast');
		loadingScreen.isLoading = true;
	};

	loadingScreen.remove = function () {
		loadingScreen.obj.fadeOut('fast', function () {
			$(this).detach();
		});
		loadingScreen.isLoading = false;
	};

	//init
	$(window).on('load', function () {
		loadingScreen.remove();
	});

	return loadingScreen;

})(jQuery);
