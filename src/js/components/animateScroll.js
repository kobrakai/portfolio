var animateScroll = (function ($) {

	'use strict';

	var animateScroll = {
		'class': 'animate'
	};

	animateScroll.animate = function (obj, trigger, offset) {
		$(window).on('scroll', function () {
			var pos = trigger.offset().top - offset;
			var currentPos = $(window).scrollTop();
			if (currentPos >= pos) {
				obj.addClass(animateScroll.class);
			}else {
				obj.removeClass(animateScroll.class);
			}
		});
	};

	//Init
	$(function () {
		animateScroll.animate($('.personal-card'), $('.about'), 600);
		animateScroll.animate($('.work .list'), $('.work .list'), 1000);
	});

	return animateScroll;

})(jQuery);
