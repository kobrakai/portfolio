var gulp 		= require('gulp'),
		config 	= require('./gulp/config'),
		plugins = require('gulp-load-plugins')({
			pattern: '*'
		});


// Get tasks
function getTask(task) {
	return require( './gulp/tasks/'+task )( gulp, plugins, config );
};

// Compile
gulp.task( 'html', getTask('compile-html') );
gulp.task( 'css', getTask('compile-css') );
gulp.task( 'js', getTask('compile-js') );

// Assets
gulp.task( 'image', getTask('compress-image') );
gulp.task( 'fonts', getTask('add-fonts') );
gulp.task( 'assets', ['image', 'fonts'] );

// Test
gulp.task( 'wcag', getTask('wcag') );

// Build
gulp.task( 'build', ['html', 'js', 'css', 'assets'] );

// Watch and serve
gulp.task( 'serve', ['build'], getTask('serve') );
gulp.task( 'default', ['serve'] );
