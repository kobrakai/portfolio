var src  = './src/';
var dest = './public/';
var dist = './assets/';

module.exports = {
	server: {
		baseDir: dest,
		proxy:  '',
		files: [
			dest + "/**/*.*",
			"!" + dest + "/**/*.map"
		],
		useProxy: false,
		https: false
	},
	css: {
		src: src + 'css/*.{sass,scss}',
		dest: dest + 'css',
		dist: dist + 'css',
		watch: src + 'css/**/*.{sass,scss}'
	},
	js: {
		src: [
			src + 'js/**/*.js'
		],
		name:{
			full: 'script.js',
			min:  '.min.js',
			map:  'script.js.map'
		},
		watch: [
			src + 'js/**/*.{js,jsx}'
		],
		dest: dest + 'js',
		dist: dist + 'js'
	},
	html: {
		src: [
			src + 'html/*.jade'
		],
		wcag: {
			src: 		dest + '*.html',
			type:  'txt',
			dest:  'reports/'
		},
		dest: dest,
		dist: dist
	},
	img: {
		src: src + 'img/*',
		dest: dest + 'img',
		dist: dist + 'img'
	},
	fonts: {
		src: src + 'fonts/**/*.{otf,eot,svg,ttf,woff,woff2}',
		dest: dest + 'fonts',
		dist: dist + 'fonts'
	}
};
