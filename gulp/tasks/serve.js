module.exports = function (gulp, plugins, config) {
  return function() {

    if (config.server.useProxy === true) {
      plugins.browserSync.init({
  			proxy: config.server.proxy,
        https: config.server.https
  		});
    }else {
      plugins.browserSync.init({
        server: {
          baseDir: config.server.baseDir
        },
        https: config.server.https
  		});
    }

		gulp.watch( config.html.src, ['html'] ).on('change', plugins.browserSync.reload);
		gulp.watch( config.js.src, ['js'] ).on('change', plugins.browserSync.reload);
    gulp.watch( config.css.watch, ['css'] );
  };
};
