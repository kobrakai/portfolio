module.exports = function( gulp, plugins, config ) {
	console.log( config.css.dest )
	return function() {
		return gulp.src( config.css.src )
			.pipe( plugins.sourcemaps.init() )
			.pipe( plugins.sassGlob() )
			.pipe( plugins.sass({
				outputStyle: 'compressed'
			}).on( 'error', plugins.sass.logError ))
			.pipe( plugins.sourcemaps.write({includeContent: false}) )
    	.pipe( plugins.sourcemaps.init({loadMaps: true}) )
			.pipe( plugins.autoprefixer({ browsers: ['last 2 version', '> 5%'] }) )
    	.pipe( plugins.sourcemaps.write('/maps') )
			.pipe( gulp.dest( config.css.dest ) )
			.pipe( gulp.dest( config.css.dist ) )
			.pipe( plugins.browserSync.stream( {match: '**/*.css'} ) );;
	};
};
