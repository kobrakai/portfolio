module.exports = function (gulp, plugins, config) {
  return function(){
		return gulp.src(config.html.wcag.src)
      .pipe(plugins.accessibility({
        force: true
      }))
      .on('error', console.log)
      .pipe(plugins.accessibility.report({reportType: config.html.wcag.type}))
      .pipe(plugins.rename({
        extname: '.'+config.html.wcag.type
      }))
      .pipe(gulp.dest(config.html.wcag.dest));
  };
};
