module.exports = function (gulp, plugins, config) {
  return function(){
    return gulp.src(config.img.src)
      .pipe(plugins.imagemin({
          progressive: true,
          use: [plugins.imageminPngquant()]
      }))
      .pipe(gulp.dest(config.img.dest))
			.pipe(gulp.dest(config.img.dist));
  };
};
