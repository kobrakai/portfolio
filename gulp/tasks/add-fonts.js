module.exports = function (gulp, plugins, config) {
  return function(){
    return gulp.src(config.fonts.src)
      .pipe(gulp.dest(config.fonts.dest))
			.pipe(gulp.dest(config.fonts.dist));
  };
};
