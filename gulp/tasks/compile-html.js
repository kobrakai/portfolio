module.exports = function (gulp, plugins, config) {
	return function(){
		return gulp.src(config.html.src)
    .pipe(plugins.jade())
    .pipe(gulp.dest(config.html.dest))
		.pipe(gulp.dest(config.html.dist));
  };
};
